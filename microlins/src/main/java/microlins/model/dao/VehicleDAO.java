/**
 * 
 */
package microlins.model.dao;

import microlins.model.Vehicle;

/**
 * @author Silvan
 *
 */
public class VehicleDAO extends GenericDAO<Vehicle> {
}
