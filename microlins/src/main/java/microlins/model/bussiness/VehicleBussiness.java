/**
 * 
 */
package microlins.model.bussiness;

import microlins.model.Vehicle;

/**
 * @author Silvan
 *
 */
public interface VehicleBussiness {
	public void salvar(Vehicle entidade);
}
