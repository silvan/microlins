package microlins.util.teste;


import org.hibernate.Session;

import microlins.model.Dispositivo;
import microlins.model.Fabricante;
import microlins.model.Person;
import microlins.model.Vehicle;
import microlins.model.dao.DispositivoDAO;
import microlins.model.dao.FabricanteDAO;
import microlins.model.dao.PersonDAO;
import microlins.model.dao.VehicleDAO;
import microlins.util.HibernateUtil;


public class HibernateUtilTest {

	public static void main(String[] args) {
		/*Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
		sessao.close();
		HibernateUtil.getFabricaDeSessoes().close();*/
		
		Vehicle vehicle = new Vehicle();
		
		vehicle.setPlaca("FUG-6563");
		vehicle.setModelo("GOL");
		
		
		DispositivoDAO  dispositivoDAO =  new DispositivoDAO();
		Dispositivo dispositivo = dispositivoDAO.buscar(1L);
		
		
		FabricanteDAO fabricanteDAO = new FabricanteDAO();
		Fabricante fabricante = fabricanteDAO.buscar(1L);
		
		
		dispositivo.setFabricante(fabricante);		
		vehicle.setDispositivo(dispositivo);
		
		PersonDAO personDAO = new PersonDAO();
		
		Person person = personDAO.buscar(1L);
		vehicle.setPerson(person);
		
		VehicleDAO vehicleDAO = new VehicleDAO();
		
		vehicleDAO.salvar(vehicle);
		
			
	}

}