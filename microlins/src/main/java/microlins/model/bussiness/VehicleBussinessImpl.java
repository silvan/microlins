/**
 * 
 */
package microlins.model.bussiness;


import org.hibernate.Session;
import org.hibernate.Transaction;

import microlins.model.Vehicle;
import microlins.util.HibernateUtil;

/**
 * @author Silvan
 *
 */
public class VehicleBussinessImpl implements VehicleBussiness{
	
	public void salvar(Vehicle entidade) {
		Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
		Transaction transacao = null;

		try {
			transacao = sessao.beginTransaction();
			sessao.save(entidade);
			transacao.commit();
		} catch (RuntimeException erro) {
			if (transacao != null) {
				transacao.rollback();
			}
			throw erro;
		} finally {
			sessao.close();
		}
	}

}
